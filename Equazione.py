#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -*-Developer:--D****-*-
# -*-Date:--28-8-2007--*-
# -*-License:--Gpl-----*-

from Tkinter import*
import math
import sys
import tkMessageBox


class Equazionando():
	
	
	def Equazione(self):
		try:
			self.Window_two = Tk()
			self.Frame_two = Frame(self.Window_two)
			self.Frame_two.pack()
			self.Label1 = Label(self.Frame_two, text = "" )
			self.Label1.pack()
			self.Label2 = Label(self.Frame_two, text = "" ) 
			self.Label2.pack()
			self.Label3 = Label(self.Frame_two, text = "" ) 
			self.Label3.pack()
			self.a = float(self.x.get())
			self.b = float(self.z.get())
			self.c = float(self.y.get())
			if self.a == 0:
				if self.b == 0: 
					if self.c == 0:
						self.Label2['text'] = "Equazione Indeterminata"
				else:
					if c == 0:
						self.Label3['text'] =  "x = 0"
					else:
						self.R = -self.c/self.b
						self.Label3['text'] =  "x = " + str(self.R)
			else:
				self.Delta = self.b**2-4*self.a*self.c
				self.Label1['text'] =  "L'equazione da risolvere e': " + str(self.a) + "x^2 +" + str(self.b) + "x +" + str(self.c)
				if self.Delta > 0:
					self.x1 = ((-self.b) + math.sqrt(self.Delta))/(2*self.a)
					self.x2 = ((-self.b) - math.sqrt(self.Delta))/(2*self.a)	
					self.Label2['text'] =  "Delta > 0, le soluzioni sono reali e distinte"
					self.Label3['text'] =  " x1 = " + str(self.x1) + "  x2 =" + str(self.x2)
				elif self.Delta < 0:
					self.Label2['text'] = "Radici complesse"
					self.x1nr = ((-self.b) + math.sqrt(-self.Delta)*(0+1j))/(2*self.a)
					self.x2nr = ((-self.b) - math.sqrt(-self.Delta)*(0+1j))/(2*self.a)
					self.Label3['text'] =  "x1 = " + str(self.x1nr) + "  x2 = " + str(self.x2nr)
				else:
					self.Labe2['text'] = "Radici reali e coincidenti"
					self.x1 = ((-self.b) + math.sqrt(self.Delta))/(2*self.a)
					self.Label3['text'] = "x1 = x2 = " + str(self.x1)
		except:
			tkMessageBox.showinfo(title = "Errore", message = "Errore: Verifica i Dati Inseriti")
		self.Window_two.mainloop()
	
	
	def Disequazione(self):
		try:
			self.Window_two = Tk()
			self.Frame_two = Frame(self.Window_two)
			self.Frame_two.pack()
			self.Label1 = Label(self.Frame_two, text = "" )
			self.Label1.pack()
			self.Label2 = Label(self.Frame_two, text = "" ) 
			self.Label2.pack()
			self.Label3 = Label(self.Frame_two, text = "" ) 
			self.Label3.pack()
			self.a = float(self.x.get())
			self.b = float(self.z.get())
			self.c = float(self.y.get())
			self.Delta = self.b**2 - (4*self.a*self.c)
			self.Label1['text'] = "La disequazione da risolvere e': "+str(self.a)+"x^2 +"+str(self.b)+"x +"+str(self.c)+"> 0"
			if self.Delta < 0 and self.a > 0:
				self.Label2['text'] = "La Soluzione e' l'insieme dei numeri R"
			elif self.Delta < 0 and self.a < 0:
				self.Label2['text'] = "La soluzione in R e' l'insieme vuoto"
			elif self.Delta >= 0 and self.a > 0:
				self.x1 = ((-self.b) + math.sqrt(self.Delta))/(2*self.a)
				self.x2 = ((-self.b) - math.sqrt(self.Delta))/(2*self.a)
				self.Label2['text'] = "La soluzione e' l'intervallo esterno delle radici"
				if self.x1 < self.x2:
					self.Label3['text'] =  "x < "+str(self.x1)+" e x > "+str(self.x2)
				else:
					self.Label3['text'] = "x < "+str(self.x2)+" e x > "+str(self.x1)
			elif delta >= 0 and a < 0:
				self.Label2['text'] = "La soluzione e' nell'intervallo interno tra le radici"
				self.x1 = ((-self.b) + math.sqrt(self.Delta))/(2*self.a)
				self.x2 = ((-self.b) - math.sqrt(self.Delta))/(2*self.a)
				if self.x1 > self.x2:
					self.Label3['text'] =  str(self.x2)+"< x <"+str(self.x1)
				else:
					self.Label3['text'] =  str(self.x1)+"< x <"+str(self.x2)
		except:
			tkMessageBox.showinfo(title = "Errore", message = "Errore: Verifica i Dati Inseriti")
		self.Window_two.mainloop()
		
	
	def __init__(self, root):
		self.root = root
		self.root.title('Equazioni')
		self.Frame_one = Frame(self.root, width = 300, height = 150)
		self.Frame_one.pack()
		Label(self.Frame_one, text = "a: ").grid(row = 0, column = 3)
		self.x = Entry(self.Frame_one, width = 5)
		self.x.grid(row = 0, column = 4 )
		Label(self.Frame_one, text = "b: ").grid(row = 1, column = 3, padx = 5 )
		self.z = Entry(self.Frame_one, width = 5)
		self.z.grid(row = 1, column = 4 )	
		Label(self.Frame_one, text = "c: ").grid(row = 2, column = 3 )
		self.y = Entry(self.Frame_one, width = 5)
		self.y.grid(row = 2, column = 4, padx = 10 )	
		self.Button_one = Button(self.Frame_one, width = 18, text = "Risolvi un' Equazione", command = self.Equazione)
		self.Button_one.grid(row = 0, column = 0)
		self.Button_two = Button(self.Frame_one, width = 18, text = "Risolvi una Disequazione", command = self.Disequazione)
		self.Button_two.grid(row = 1, column = 0 )

Window_one = Tk()
App = Equazionando(Window_one)
Window_one.mainloop() 
